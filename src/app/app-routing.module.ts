import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './page/admin/admin.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DesignpeperComponent } from './components/designpeper/designpeper.component';
import { RoomComponent } from './components/room/room.component';


const routes: Routes = [
  {
    path: '', 
    redirectTo: 'home', 
    pathMatch: 'full' 
  },
  { 
    path: 'home', 
    component: HomeComponent ,
  },
  { 
    path: 'adminPage', 
    component: AdminComponent ,
  },
  { 
    path: 'loginComponent', 
    component: LoginComponent ,
  },
  { 
    path: 'registerComponent', 
    component: RegisterComponent,
  },
  {
    path: 'admin',
    component: AdminComponent,
    outlet: 'adminOutlet'
  },
  {
    path: 'profileComponent',
    component: ProfileComponent,
    // outlet: 'admin'
  },
  {
    path: 'designPeper-Component',
    component: DesignpeperComponent,
    // outlet: 'admin'
  },
  {
    path: 'Room-Component',
    component: RoomComponent,
    // outlet: 'admin'
  },


  
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
