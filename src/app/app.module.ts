import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, 
        NbLayoutModule,
        NbButtonModule,
        NbIconModule,
        NbSidebarModule,
        NbCardModule,
        NbInputModule,
        NbActionsModule,
        NbTabsetModule,
        NbTooltipModule,
        NbMenuModule,
        NbTreeGridModule
       } from '@nebular/theme';

import { NbEvaIconsModule } from '@nebular/eva-icons';
import { AdminComponent } from './page/admin/admin.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { HomesComponent } from './page/homes/homes.component';
import { ProfileComponent } from './components/profile/profile.component';
import { DesignpeperComponent } from './components/designpeper/designpeper.component';
import { RoomComponent } from './components/room/room.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { Ng2CompleterModule } from "ng2-completer";



@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    HomesComponent,
    ProfileComponent,
    DesignpeperComponent,
    RoomComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NbThemeModule.forRoot({ name: 'default'  }),
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbIconModule,
    NbSidebarModule.forRoot(), 
    NbCardModule,
    NbInputModule,
    NbActionsModule,
    NbTabsetModule,
    NbTooltipModule,
    NbMenuModule.forRoot(),
    Ng2SmartTableModule,
    Ng2CompleterModule,
    NbTreeGridModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { 
}

