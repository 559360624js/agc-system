import { Component, OnInit } from '@angular/core';
import { NbSidebarService } from '@nebular/theme';
import { Router } from '@angular/router';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  
  items = [
    {
      title:'ข้อมูลผู้ใช้',
      icon: 'person-outline',
      link: 'profileComponent',
    },
    {
      title: 'ออกแบบข้อสอบ',
      icon: 'edit-2-outline',
      link: 'designPeper-Component',
    },
    {
      title: 'ห้องเรียน',
      icon: 'file-add-outline',
      link: 'Room-Component',
    },
    {
      title: 'ตั้งค่า',
      icon: 'settings-2-outline',
      link: [],
    },

  ];


  constructor(private sidebarService: NbSidebarService,private router: Router) {
    
   }

  ngOnInit() {
  }
  toggle() {
    this.sidebarService.toggle(true);
    return false;
  }

}
