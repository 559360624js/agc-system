import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DesignpeperComponent } from './designpeper.component';

describe('DesignpeperComponent', () => {
  let component: DesignpeperComponent;
  let fixture: ComponentFixture<DesignpeperComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DesignpeperComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DesignpeperComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
