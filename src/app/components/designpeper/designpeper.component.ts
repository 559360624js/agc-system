import { Component,OnInit } from '@angular/core';

@Component({
  selector: 'app-designpeper',
  templateUrl: './designpeper.component.html',
  styleUrls: ['./designpeper.component.scss']
})
export class DesignpeperComponent implements OnInit {

ngOnInit() {
    // this.addimg();
}

// addimg(){
//     window.onload = function () {

//         // GET THE IMAGE.
//         var img = new Image();
//         img.src = 'https://www.encodedna.com/images/hawa-mahal.jpg';

//         // WAIT TILL IMAGE IS LOADED.
//         img.onload = function () {
//             fill_canvas(img);       // FILL THE CANVAS WITH THE IMAGE.
//         }

//         function fill_canvas(img) {
//             // CREATE CANVAS CONTEXT.
//             var canvas = <HTMLCanvasElement> document.getElementById('canvas');
//             var ctx = canvas.getContext('2d');

//             canvas.width = 1200;
//             canvas.height = 500;

//             ctx.drawImage(img, 0, 0); // DRAW THE IMAGE TO THE CANVAS.

            
//         }
//     }
// }

selectData(){
        var img = new Image();
        img.src = '../assets/test.jpg';
        // img.onload = () => {
        //     ctx.drawImage(img, 0, 0);
        // };
        var canvas = <HTMLCanvasElement> document.getElementById('canvas'),
        ctx = canvas.getContext('2d'),
        rect = {
          startX: 200,
          startY: 200,
          w: 150,
          h: 150
      },
        drag = false,
        mouseX,
        mouseY,
        closeEnough = 10,
        dragTL =false , 
        dragBL = false, 
        dragTR =false , 
        dragBR = false;

        canvas.width = img.width;
        canvas.height = img.height;

    // function drawImage(img){
    //     ctx.drawImage(img, 10, 10); 
    // }


    function init() {

        canvas.addEventListener('mousedown', mouseDown, false);
        canvas.addEventListener('mouseup', mouseUp, false);
        canvas.addEventListener('mousemove', mouseMove, false);
        this.drawImage();

    }

    function mouseDown(e) {
        mouseX = e.pageX - this.offsetLeft;
        mouseY = e.pageY - this.offsetTop;

        // if there isn't a rect yet
        if (rect.w === undefined) {
            rect.startX = mouseY;
            rect.startY = mouseX;
            dragBR = true;
        }

        // if there is, check which corner
        //   (if any) was clicked
        //
        // 4 cases:
        // 1. top left
        else if (checkCloseEnough(mouseX, rect.startX) && checkCloseEnough(mouseY, rect.startY)) {
            dragTL = true;
        }
        // 2. top right
        else if (checkCloseEnough(mouseX, rect.startX + rect.w) && checkCloseEnough(mouseY, rect.startY)) {
            dragTR = true;

        }
        // 3. bottom left
        else if (checkCloseEnough(mouseX, rect.startX) && checkCloseEnough(mouseY, rect.startY + rect.h)) {
            dragBL = true;

        }
        // 4. bottom right
        else if (checkCloseEnough(mouseX, rect.startX + rect.w) && checkCloseEnough(mouseY, rect.startY + rect.h)) {
            dragBR = true;

        }
        // (5.) none of them
        else {
            // handle not resizing
        }

        ctx.clearRect(0, 0, canvas.width, canvas.height);
        draw();

    }

    function checkCloseEnough(p1, p2) {
        return Math.abs(p1 - p2) < closeEnough;
    }

    function mouseUp() {
        dragTL = dragTR = dragBL = dragBR = false;
    }

    function mouseMove(e) {
        mouseX = e.pageX - this.offsetLeft;
        mouseY = e.pageY - this.offsetTop;
        if (dragTL) {
            rect.w += rect.startX - mouseX;
            rect.h += rect.startY - mouseY;
            rect.startX = mouseX;
            rect.startY = mouseY;
        } else if (dragTR) {
            rect.w = Math.abs(rect.startX - mouseX);
            rect.h += rect.startY - mouseY;
            rect.startY = mouseY;
        } else if (dragBL) {
            rect.w += rect.startX - mouseX;
            rect.h = Math.abs(rect.startY - mouseY);
            rect.startX = mouseX;
        } else if (dragBR) {
            rect.w = Math.abs(rect.startX - mouseX);
            rect.h = Math.abs(rect.startY - mouseY);
        }
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        draw();
    }

    function draw() {
        ctx.drawImage(img, 1, 1);
        ctx.fillStyle = "rgba(0, 162, 255, 0.205)";
        ctx.fillRect(rect.startX, rect.startY, rect.w, rect.h);
        drawHandles();
    }
   
function drawCircle(x, y, radius) {
    ctx.fillStyle = "rgb(255, 255, 255)";
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI);
    ctx.fill();
}

function drawHandles() {
    drawCircle(rect.startX, rect.startY, closeEnough);
    drawCircle(rect.startX + rect.w, rect.startY, closeEnough);
    drawCircle(rect.startX + rect.w, rect.startY + rect.h, closeEnough);
    drawCircle(rect.startX, rect.startY + rect.h, closeEnough);
}
    init();

}
  constructor() { }



}
